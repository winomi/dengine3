#FROM node:10.15.3-slim@sha256:5a48598688f771a9140fdef31585dbe67b313b1e29439cbd9b81ebb25aeca517
#FROM node:10-alpine AS multistage
FROM node:10-alpine 

#RUN apk update && apk upgrade 
RUN apk --update --no-cache add msttcorefonts-installer fontconfig && \
    update-ms-fonts && \
    fc-cache -f

#RUN apk add --update --no-cache \
#    python \
#    make \
#    g++

#RUN  apt-get update \
     # See https://crbug.com/795759
#     && apt-get install -yq libgconf-2-4 \
     # Install latest chrome dev package, which installs the necessary libs to
     # make the bundled version of Chromium that Puppeteer installs work.
     #&& apt-get install -y wget --no-install-recommends \
     #&& wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
     #&& sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
#     && apt-get update \
#     && apt-get install -y fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst ttf-freefont --no-install-recommends \
#     && rm -rf /var/lib/apt/lists/* \
#     && wget --quiet https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh -O /usr/sbin/wait-for-it.sh \
#     && chmod +x /usr/sbin/wait-for-it.sh 

# Installs latest Chromium (72) package.
#RUN apk update && apk upgrade && \
RUN    echo @edge http://nl.alpinelinux.org/alpine/edge/community >> /etc/apk/repositories && \
    echo @edge http://nl.alpinelinux.org/alpine/edge/main >> /etc/apk/repositories && \
#    apk add --no-cache \
    apk add --update --no-cache \
      chromium@edge \
      nss@edge \
      freetype@edge \
      harfbuzz@edge \
      ttf-freefont@edge


# Tell Puppeteer to skip installing Chrome. We'll be using the installed package.
#ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true

# Puppeteer v1.11.0 works with Chromium 72.
#RUN npm install --save puppeteer@0.11.0
# Install pm2
RUN npm install pm2 -g

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
WORKDIR /home/node/app
COPY package*.json ./    

COPY ecosystem.config.js .
# Install app dependencies
ENV NPM_CONFIG_LOGLEVEL warn

USER node
RUN npm install
#RUN npm ci
COPY --chown=node:node . .
EXPOSE 3001
#RUN npm run format
#RUN npm run build
#RUN npm run test
#RUN npm prune --production

#CMD [ "node", "server.js" ]
# Show current folder structure in logs
RUN ls -al -R

CMD [ "pm2-runtime", "start", "ecosystem.config.js" ]

#FROM node:10-alpine

#EXPOSE 3009
#WORKDIR /home/node
#COPY --from=multistage /home/node/app app
#COPY --from=build /home/node/app/dist dist
#USER node
#CMD ["node", "./app/server.js"]
