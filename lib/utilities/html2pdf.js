const fs = require('fs-extra');
const Boom = require('boom');
//const puppeteer = require('puppeteer');
const puppeteer = require('puppeteer-core');
const hummus = require('hummus');
const mstreams = require('memory-streams');
const uuidv4 = require('uuid/v4');

const auditHtml = require(global.appRoot + '/lib/utilities/audit2html.js');

module.exports = async function(images, audit){  
    let tempDirPath = global.appRoot + '/temp/'; 
    let tempPdfPath = tempDirPath+ '/tmp/' + uuidv4();
    let tempPdfPath0 = tempPdfPath + '_0.pdf';
    let tempPdfPath1 = tempPdfPath + '_1.pdf';
    let tempHtmlPath = tempDirPath + '/temp1.html';


    try {
        const browser = await puppeteer.launch({
            executablePath: '/usr/bin/chromium-browser',
            args: [
                '--no-sandbox', 
                '--disable-setuid-sandbox', 
            //    '--font-render-hinting=medium', 
                '--enable-font-antialiasing',
                '--disable-dev-shm-usage',
                '--headless', 
                '--disable-gpu'
            ],
            timeout:60000,
            headless: true
        });
        let pdfHtml = createPdf(images, audit);

        //await fs.writeFile(tempHtmlPath, pdfHtml);

        let page = await browser.newPage();
        //await page.setViewport({width:1440, height:900, deviceScaleFactor: 2});
        await page.setContent(pdfHtml);
        //await page.emulateMedia('screen');
        let pdf = await page.pdf({
            //printBackground: true,
            //preferCSSPageSize: true,
            fomat: 'A4'
        });

        await browser.close();

        await fs.writeFile(tempPdfPath0, pdf);

        //encrypt
        //let outStream = new mstreams.WritableStream();
        //let pdfDoc = new hummus(tempPdfPath, tempPdfPath1);
        //let pdfDoc = new hummus(pdf, outStream);
        //pdfDoc.encrypt(
        await hummus.recrypt(
            //new hummus.PDFRStreamForBuffer(pdf),//tempPdfPath,
            //new hummus.PDFWStreamForFile(tempPdfPath1),//tempPdfPath1,
            //new hummus.PDFStreamForResponse(outStream),//tempPdfPath1,
            tempPdfPath0,
            tempPdfPath1,
            //userPassword: '123',
            //ownerPassword: '123',
            {
                userPassword: '',
                //ownerPassword: '123',
                userProtectionFlag: 4
            }
        );
        //.endPDF();

        //outStream.end();
        //fs.writeFileSync(tempPdfPath, outStream.toBuffer());
        //let p = await fs.createReadStream(tempPdfPath1);
        let p = await fs.readFile(tempPdfPath1);

        await fs.unlink(tempPdfPath0);
        await fs.unlink(tempPdfPath1);

        let fResponse = {
            statusCode: 200,
            pdf: p.toString('base64')
            //pdf: outStream.toString('base64')
        }
        return fResponse;
        //return p;//.pipe(outStream).toBuffer();//outStream.toBuffer();//pdf;//fResponse;
        //return outStream.toBuffer();
    }
    catch (err) {
        console.error(err);
        throw Boom.badImplementation('problem with html/pdf engine ' + err.message);

    }
}

function createPdf(images, audit){
    let html = "";
    html += '<html>' +
            '<style>' +
                'img {' +
                    'max-width: 100%;' +
                    'max-height: 100%;' +
                    'display: block;' +
                    'margin-left: auto;' +
                    'margin-right: auto;' +
                '}' +
                '.head {' +
                    'font-size: 18;' +
                    'margin-top: 30pt;' +
                    'margin-left: 20pt;' +
                    'font-weight: 300;' +
                    'font-family: Arial, Helvetica, sans-serif;' +
                '}' +
                '.normal {' +
                    'font-size: 12;' +
                    'margin-top: 5pt;' +
                    'margin-left: 20pt;' +
                    'font-weight: 300;' +
                    'font-family: Arial, Helvetica, sans-serif;' +
                '}' +
                '.subnormal {' +
                    'font-size: 10;' +
                    'margin-top: 5pt;' +
                    'margin-left: 20pt;' +
                    'font-weight: 300;' +
                    'font-family: Arial, Helvetica, sans-serif;' +
                '}' +
                '.img1 {' +
                    //'width:595.4pt;' +
                    'width:auto;' +
                    'height:786pt;' +
                    'font-size: 0;' +
                    'margin: auto;' +
                    'padding: 0;' +
                '}' +
                //'@page {' +
                //    'margin: 0;' +
                //    'padding: 0;' +
                //'}' +
                //'html, body {' +
                    //'height: 1590pt;' +
                    //'margin: 0;' +
                    //'padding: 0;' +
                    //'font-size: 0;' +
                //'}' +                
            '</style>';
    html += '<body>';
    for (let ii = 0; ii < images.length; ii++){
        html += '<div class="img1">';// style="page-break-after:always;">';
        html += '<img src="data:image/jpg;base64, ';
        html += images[ii].toString('base64');
        html += '" />';
        html += '</div>';
    }

    html += auditHtml(audit, images.length);

    html += '</body>' +
            '</html>';

    return html;
}

